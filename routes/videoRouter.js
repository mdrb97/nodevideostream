const express = require('express');
const router = express.Router();

/* GET users listing. */
router.get('/:filename', function(req, res) {
    let obj = {}
    obj.videoLocation = req.params.filename
    res.render('video', obj)
});



module.exports = router