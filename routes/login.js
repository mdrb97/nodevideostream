var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  req.user = "LOGGEDTEST"
  res.render('loginpage', { title: 'Stream! Login' });
});

module.exports = router;
