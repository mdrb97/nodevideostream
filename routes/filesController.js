var express = require('express');
var router = express.Router();
const fs = require('fs')
let folderMap = {
    firstFolder : "C:\\Users\\Mark0\\WebstormProjects\\StreamingTest\\assets\\firstFolder\\",
    secondFolder : "C:\\Users\\Mark0\\WebstormProjects\\StreamingTest\\assets\\secondFolder\\"
}

let fileMap = {}

/* GET users listing. */
router.get('/:foldername', function(req, res) {
    let listOfFiles = [];
    let listOfFolders = [];
    let choosenFolder = folderMap[req.params.foldername]
    fs.readdir(choosenFolder, (err, files) => {
        files.forEach((file) => {
            if(fs.lstatSync(choosenFolder + file).isDirectory()){
                let splitDir = file.split('/');
                let lastPartOfDir = splitDir[splitDir.length-1]
                folderMap[lastPartOfDir] = choosenFolder + file + '\\';
                listOfFolders.push(file);
            }
            else {
                listOfFiles.push(file)
                fileMap[file] = choosenFolder + file;
            }
        })
    })

    let obj = {}
    obj.folderList = listOfFolders
    obj.fileList = listOfFiles



    res.render('filesview', obj)


});

function getFileMap(){
    return fileMap
}

module.exports =
    {router : router,
    getFileMap : getFileMap
};